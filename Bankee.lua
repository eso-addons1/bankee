Bankee = Bankee or {}

Bankee.name = "Bankee"
Bankee.title = "Bankee"
Bankee.version = "1.0"
Bankee.author = "Thenedus"

-- Stores all classes used by this addon to prevent polluting the global namespace
Bankee.classes = {}

Bankee.defaults = {
    direction = Bankee.StackDirection.TO_BANK,
    printToChat = true,
    categories = {}
}

LibLanguage.LoadLanguage(Bankee_Localization, "en")

local function OnAddonLoaded(eventCode, addOnName)
    if ( addOnName ~= Bankee.name ) then return end
    EVENT_MANAGER:UnregisterForEvent(Bankee.name, eventCode)

    Bankee.classes.Settings.Initialize()
    Bankee.classes.KeyStripButton.Initialize()
end

EVENT_MANAGER:RegisterForEvent(Bankee.name, EVENT_ADD_ON_LOADED, OnAddonLoaded)
