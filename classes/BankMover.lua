local addon = Bankee

addon.classes.BankMover = {}

local itemTypesByCategory = {
    [addon.Categories.BLACKSMITHING] = { ITEMTYPE_BLACKSMITHING_RAW_MATERIAL, ITEMTYPE_BLACKSMITHING_MATERIAL, ITEMTYPE_BLACKSMITHING_BOOSTER },
    [addon.Categories.CLOTHIER] = { ITEMTYPE_CLOTHIER_RAW_MATERIAL, ITEMTYPE_CLOTHIER_MATERIAL, ITEMTYPE_CLOTHIER_BOOSTER },
    [addon.Categories.WOODWORKING] = { ITEMTYPE_WOODWORKING_RAW_MATERIAL, ITEMTYPE_WOODWORKING_MATERIAL, ITEMTYPE_WOODWORKING_BOOSTER },
    [addon.Categories.ALCHEMY] = {{ ITEMTYPE_REAGENT, ITEMTYPE_POTION_BASE, ITEMTYPE_POISON_BASE }},
    [addon.Categories.ENCHANTING] = {{ ITEMTYPE_ENCHANTING_RUNE_ASPECT, ITEMTYPE_ENCHANTING_RUNE_ESSENCE, ITEMTYPE_ENCHANTING_RUNE_POTENCY }},
    [addon.Categories.PROVISIONING] = { ITEMTYPE_INGREDIENT },
    [addon.Categories.JEWELRY] = { ITEMTYPE_JEWELRYCRAFTING_RAW_MATERIAL, ITEMTYPE_JEWELRYCRAFTING_MATERIAL, { ITEMTYPE_JEWELRYCRAFTING_BOOSTER, ITEMTYPE_JEWELRYCRAFTING_RAW_BOOSTER } },
    [addon.Categories.STYLEITEMS] = {{ ITEMTYPE_STYLE_MATERIAL, ITEMTYPE_RAW_MATERIAL }},
    [addon.Categories.TRAITITEMS] = {{ ITEMTYPE_WEAPON_TRAIT, ITEMTYPE_ARMOR_TRAIT, ITEMTYPE_JEWELRY_TRAIT, ITEMTYPE_JEWELRY_RAW_TRAIT }}
}

local function FlattenTable(tbl)
    -- copied from https://github.com/premake/premake-4.x/blob/master/src/base/table.lua
    local result = { }

    local function flatten(arr)
        for _, v in ipairs(arr) do
            if type(v) == "table" then
                flatten(v)
            else
                table.insert(result, v)
            end
        end
    end

    flatten(tbl)
    return result
end

local function ParseRule(rule)
    local itemTypesForCategory = itemTypesByCategory[rule.category]
    local itemTypes
    if (rule.subcategory == addon.SubCategories.ALL) then
        itemTypes = itemTypesForCategory
    elseif rule.subcategory == addon.SubCategories.RAW_MATERIAL then
        itemTypes = itemTypesForCategory[1]
    elseif rule.subcategory == addon.SubCategories.MATERIAL then
        itemTypes = itemTypesForCategory[2]
    elseif rule.subcategory == addon.SubCategories.BOOSTER then
        itemTypes = itemTypesForCategory[3]
    end

    if itemTypes == nil then return nil end
    if type(itemTypes) ~= "table" then
        itemTypes = { itemTypes }
    end

    local fullStackOnly = rule.stackOption == addon.StackOptions.FULL_STACK

    return FlattenTable(itemTypes), fullStackOnly
end

local function FilterByItemTypes(item, itemTypes)
    local link = GetItemLink(item.bagId, item.slotIndex, LINK_STYLE_DEFAULT)
    local itemType = GetItemLinkItemType(link)

    return ZO_IsElementInNumericallyIndexedTable(itemTypes, itemType)
end

local function FilterByStack(item, fullStackOnly)
    if not fullStackOnly then return true end

    local quantity, maxQuantity = GetSlotStackSize(item.bagId, item.slotIndex)
    return quantity == maxQuantity
end

local function MoveByRule(rule, source, destinationBag)
    local itemTypes, fullStackOnly = ParseRule(rule)
    if itemTypes == nil then return end

    function Filter(item)
        return addon.classes.ItemUtils.IsMovableItemType(item)
            and FilterByItemTypes(item, itemTypes)
            and FilterByStack(item, fullStackOnly)
    end

    local sourceBag = LibBag.Bag:New(source, Filter)

    for _, sourceItem in pairs(sourceBag.items) do
        local movedAmount = sourceItem:MoveWholeStackToBag(destinationBag)
        addon.classes.ItemUtils.PrintMovedItem(sourceItem, movedAmount)
    end
end

function addon.classes.BankMover.MoveByRules(rules, source, destination)
    if rules == nil or type(rules) ~= "table" then return end

    -- create the target bag once to ensure to use the same instance
    -- as the target of all move operations (to ensure that
    -- FindNextFreeSlot works as expected).
    local destinationBag = LibBag.Bag:New(destination)

    for _, rule in ipairs(rules) do
        MoveByRule(rule, source, destinationBag)
    end
end