local addon = Bankee

addon.classes.CategorySelectionSettings = {}

-- layout settings
local buttonSize = 26
local horizontalSpacing = 10
local verticalSpacing = 5
local controlHeight = 30

local craftSubtypes = {
    { key = addon.SubCategories.ALL, name = GetString(BANKEE_SI_SETTINGS_SUBCATEGORY_ALL) },
    { key = addon.SubCategories.RAW_MATERIAL, name = GetString(SI_SPECIALIZEDITEMTYPE1500) },
    { key = addon.SubCategories.MATERIAL, name = GetString(SI_SPECIALIZEDITEMTYPE1550) },
    { key = addon.SubCategories.BOOSTER, name = GetString(BANKEE_SI_SETTINGS_SUBCATEGORY_BOOSTERS) }
}

local onlyAllSubtypes = {
    { key = addon.SubCategories.ALL, name = GetString(BANKEE_SI_SETTINGS_SUBCATEGORY_ALL) }
}

local categories = {
    { key = addon.Categories.BLACKSMITHING, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY10), subtypes = craftSubtypes },
    { key = addon.Categories.CLOTHIER, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY11), subtypes = craftSubtypes },
    { key = addon.Categories.WOODWORKING, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY12), subtypes = craftSubtypes },
    { key = addon.Categories.JEWELRY, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY13), subtypes = craftSubtypes },
    { key = addon.Categories.ALCHEMY, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY14), subtypes = onlyAllSubtypes },
    { key = addon.Categories.ENCHANTING, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY15), subtypes = onlyAllSubtypes },
    { key = addon.Categories.PROVISIONING, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY16), subtypes = onlyAllSubtypes },
    { key = addon.Categories.STYLEITEMS, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY17), subtypes = onlyAllSubtypes },
    { key = addon.Categories.TRAITITEMS, name = GetString(SI_ITEMTYPEDISPLAYCATEGORY18), subtypes = onlyAllSubtypes },
}

local stackOptions = {
    { key = addon.StackOptions.ALL_ITEMS, name = "All Items" },
    { key = addon.StackOptions.FULL_STACK, name = "Full Stacks" }
}

local selectionOptionsContainer = nil
local addButton = nil
local selectionRows = {}
local comboboxWidth = 0

local comboboxCount = 1

-- keep removed rows to reuse them
-- a custom implementation was chosen over ZO_ObjectPool as managing indexes and keys
-- for each row would have been cumbersome
local recycleBin = {}

local function UpdateComboboxOptions(combobox, options, changeFunction, initialKey)
    local dropdown = ZO_ComboBox_ObjectFromContainer(combobox)
    dropdown:ClearItems()
    dropdown:SetSortsItems(false)

    if options == nil then
        dropdown:SetSelectedItemText("")
        return
    end

    function OnSelect(_, _, choice)
        local rowIndex = combobox:GetParent().index
        changeFunction(choice.value, rowIndex)
    end

    local initialText = nil
    for _, option in ipairs(options) do
        local entry = dropdown:CreateItemEntry(option.name, OnSelect)
        entry.value = option.key
        dropdown:AddItem(entry, ZO_COMBOBOX_SUPRESS_UPDATE)

        if option.key == initialKey then
            initialText = option.name
        end
    end

    if initialText ~= nil then
        dropdown:SetSelectedItemText(initialText)
    end
end

local function CreateCombobox(container)
    local name = addon.name .. "combobox" .. comboboxCount
    comboboxCount = comboboxCount + 1
    local combobox = WINDOW_MANAGER:CreateControlFromVirtual(name, container, "ZO_ComboBox")
    combobox:SetDimensions(comboboxWidth, controlHeight)

    return combobox
end

local function HandleSubcategorySelected(subcategory, rowIndex)
    addon.settings.categories[rowIndex].subcategory = subcategory
end

local function HandleStackOptionChanged(stackOption, rowIndex)
    addon.settings.categories[rowIndex].stackOption = stackOption
end

local function UpdateSubcategoryOptions(categoryValue, rowIndex, selectedSubcategory)
    -- find the category definition for the selected key
    local selectedCategory
    for _, category in ipairs(categories) do
        if category.key == categoryValue then
            selectedCategory = category
            break
        end
    end

    if selectedCategory ~= nil then
        local matchingSelectionRow = selectionRows[rowIndex]
        local subselect = matchingSelectionRow.subTypeSelection
        UpdateComboboxOptions(subselect, selectedCategory.subtypes, HandleSubcategorySelected, selectedSubcategory)
    end
end

local function UpdateStackOptions(rowIndex, selectedStackOption)
    local selectionRow = selectionRows[rowIndex]
    local stackOptionsCombobox = selectionRow.stackOption
    UpdateComboboxOptions(stackOptionsCombobox, stackOptions, HandleStackOptionChanged, selectedStackOption)
end

local function HandleCategorySelected(categoryValue, rowIndex)
    local rowData = addon.settings.categories[rowIndex]
    rowData.category = categoryValue
    rowData.subcategory = rowData.subcategory or addon.SubCategories.ALL
    rowData.stackOption = rowData.stackOption or addon.StackOptions.ALL_ITEMS

    UpdateSubcategoryOptions(categoryValue, rowIndex, rowData.subcategory)
    UpdateStackOptions(rowIndex, rowData.stackOption)
end

local function PositionCategorySelectionControls()
    addButton:ClearAnchors()
    addButton:SetAnchor(RIGHT, selectionOptionsContainer, RIGHT, horizontalSpacing, 0, ANCHOR_CONSTRAINS_X)

    if #selectionRows == 0 then
        -- no selection rows, position the add button at the top of the container
        addButton:SetAnchor(TOP, selectionOptionsContainer, TOP, 0, 0, ANCHOR_CONSTRAINS_Y)
    else
        -- position the first selection row at the top of the container
        selectionRows[1]:ClearAnchors()
        selectionRows[1]:SetAnchor(TOPLEFT, selectionOptionsContainer, TOPLEFT, verticalSpacing, 0)

        -- position the remaining selection rows below each other
        for i = 2, #selectionRows do
            selectionRows[i]:ClearAnchors()
            selectionRows[i]:SetAnchor(TOPLEFT, selectionRows[i - 1], BOTTOMLEFT, 0, verticalSpacing)
        end

        addButton:SetAnchor(TOP, selectionRows[#selectionRows], BOTTOM, 0, verticalSpacing * 4, ANCHOR_CONSTRAINS_Y)
    end
end

local function DeleteCategory(index)
    table.remove(addon.settings.categories, index)

    local row = table.remove(selectionRows, index)
    row:SetHidden(true)
    row:ClearAnchors()
    row.index = nil

    -- renumber remaining rows
    for i = index, #selectionRows do
        selectionRows[i].index = i
    end

    table.insert(recycleBin, row)
    PositionCategorySelectionControls()
end

local function AddCategorySelectionOption(category, subcategory, selectedStackOption)
    local container
    if (#recycleBin > 0) then
        container = table.remove(recycleBin)
        container:SetHidden(false)
        UpdateComboboxOptions(container.subTypeSelection, nil)
        UpdateComboboxOptions(container.stackOption, nil)
    else
        container = WINDOW_MANAGER:CreateControl(nil, selectionOptionsContainer, CT_CONTROL)
        container:SetWidth(selectionOptionsContainer:GetWidth())
        container:SetResizeToFitDescendents(true)

        local combobox = CreateCombobox(container)
        combobox:ClearAnchors()
        combobox:SetAnchor(TOPLEFT, container, TOPLEFT, 0, 0)
        container.typeSelection = combobox

        local subtypeCombobox = CreateCombobox(container)
        subtypeCombobox:ClearAnchors()
        subtypeCombobox:SetAnchor(TOPLEFT, combobox, TOPRIGHT, horizontalSpacing, 0)
        container.subTypeSelection = subtypeCombobox

        local stackOptionCombobox = CreateCombobox(container)
        stackOptionCombobox:ClearAnchors()
        stackOptionCombobox:SetAnchor(TOPLEFT, subtypeCombobox, TOPRIGHT, horizontalSpacing, 0)
        container.stackOption = stackOptionCombobox

        local deleteButton = WINDOW_MANAGER:CreateControl(nil, container, CT_BUTTON)
        deleteButton:SetDimensions(buttonSize, buttonSize)
        deleteButton:SetNormalTexture("esoui/art/buttons/decline_up.dds")
        deleteButton:SetPressedTexture("esoui/art/buttons/decline_down.dds")
        deleteButton:SetMouseOverTexture("esoui/art/buttons/decline_up.dds")
        deleteButton:SetAnchor(LEFT, stackOptionCombobox, RIGHT, horizontalSpacing, 0)
        deleteButton:SetHandler("OnClicked", function(button) DeleteCategory(button:GetParent().index) end)
        container.deleteButton = deleteButton
    end

    container.index = #selectionRows + 1
    table.insert(selectionRows, container)

    UpdateComboboxOptions(container.typeSelection, categories, HandleCategorySelected, category)
    UpdateSubcategoryOptions(category, container.index, subcategory)

    if category ~= nil then
        UpdateStackOptions(container.index, selectedStackOption)
    end
end

local function HandleAddCategoryButtonClicked()
    table.insert(addon.settings.categories, {})
    AddCategorySelectionOption()
    PositionCategorySelectionControls()
end

local function InitializeFromSettings()
    if addon.settings.categories == nil then return end

    for _, value in ipairs(addon.settings.categories) do
        AddCategorySelectionOption(value.category, value.subcategory, value.stackOption)
    end
end

local function ComboboxWidth(container)
    local totalSpacing = 3 * horizontalSpacing
    local spaceForComboboxes = container:GetWidth() - totalSpacing - buttonSize
    return math.floor(spaceForComboboxes / 3)
end

function addon.classes.CategorySelectionSettings.BuildCategorySelectionOptions(container)
    selectionOptionsContainer = container

    comboboxWidth = ComboboxWidth(container)

    addButton = WINDOW_MANAGER:CreateControlFromVirtual(nil, container, "ZO_DefaultButton")
    addButton:SetWidth(container:GetWidth() / 3)
    addButton:SetText(GetString(SI_GAMEPAD_TRADE_ADD))
    addButton:SetClickSound("Click")
    addButton:SetHandler("OnClicked", HandleAddCategoryButtonClicked)

    InitializeFromSettings()
    PositionCategorySelectionControls()
end
