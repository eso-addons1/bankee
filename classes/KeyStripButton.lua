local addon = Bankee

addon.classes.KeyStripButton = {}

local function IsStackingToBank()
    return addon.settings.direction == Bankee.StackDirection.TO_BANK
end

local function DetermineButtonText()
    if IsStackingToBank() then
        return GetString(BANKEE_SI_STACK_TO_BANK)
    else
        return GetString(BANKEE_SI_STACK_TO_BACKPACK)
    end
end

local function Stack()
    if (addon.classes.KeyStripButton.bankBag == nil) then return end

    local sourceBag, destinationBag
    if IsStackingToBank() then
        sourceBag = BAG_BACKPACK
        destinationBag = addon.classes.KeyStripButton.bankBag
    else
        sourceBag = addon.classes.KeyStripButton.bankBag
        destinationBag = BAG_BACKPACK
    end

    Bankee.classes.BankStacker.Stack(sourceBag, destinationBag)
    Bankee.classes.BankMover.MoveByRules(addon.settings.categories, sourceBag, destinationBag)
end

local buttonDescriptor = {
    name = DetermineButtonText,
    keybind = "UI_SHORTCUT_QUATERNARY",
    callback = Stack,
    alignment = KEYBIND_STRIP_ALIGN_LEFT
}

local function ShowButton()
	KEYBIND_STRIP:AddKeybindButton(buttonDescriptor)
end

local function HideButton()
	KEYBIND_STRIP:RemoveKeybindButton(buttonDescriptor)
end

function addon.classes.KeyStripButton.Initialize()
    EVENT_MANAGER:RegisterForEvent(Bankee.Name, EVENT_OPEN_BANK, function (_, bankBag)
        addon.classes.KeyStripButton.bankBag = bankBag
        ShowButton()
    end)

    EVENT_MANAGER:RegisterForEvent(Bankee.Name, EVENT_CLOSE_BANK, HideButton)
end
