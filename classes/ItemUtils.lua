local addon = Bankee

addon.classes.ItemUtils = {}

function addon.classes.ItemUtils.IsMovableItemType(item)
    return not (
        item.itemType == ITEMTYPE_FOOD
        or item.itemType == ITEMTYPE_DRINK
        or item.itemType == ITEMTYPE_POTION
        or item.itemType == ITEMTYPE_SOUL_GEM
        or item.itemType == ITEMTYPE_TOOL
    )
end

function addon.classes.ItemUtils.PrintMovedItem(item, movedAmount)
    if addon.settings.printToChat then
        local messageTemplate = GetString(BANKEE_SI_MOVED_ITEM_MESSAGE)
        CHAT_SYSTEM:AddMessage(zo_strformat(messageTemplate, item:Link(), movedAmount, item.quantity))
    end
end
