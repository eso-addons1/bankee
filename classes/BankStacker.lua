local addon = Bankee

addon.classes.BankStacker = {}

local function IsValidItem(item)
    return addon.classes.ItemUtils.IsMovableItemType(item) -- and not item.slotData.stolen and not item.slotData.isPlayerLocked
end

function addon.classes.BankStacker.Stack(source, destination)
    local sourceBag = LibBag.Bag:New(source, IsValidItem)
    local destinationBag = LibBag.Bag:New(destination)

    for _, sourceItem in pairs(sourceBag.items) do
        if sourceItem.quantity < sourceItem.maxQuantity then
            -- skip full stacks
            local movedAmount, _ = sourceItem:StackToBag(destinationBag)
            if movedAmount > 0 then
                addon.classes.ItemUtils.PrintMovedItem(sourceItem, movedAmount)
            end
        end
    end
end
