local addon = Bankee

addon.classes.Settings = {}

local function SetupSavedVars()
    addon.settings = ZO_SavedVars:NewCharacterIdSettings("Bankee_Data", 1, nil, Bankee.defaults, GetWorldName())
end

local function SetupSettingsPanel()
    local panelData = {
        type = "panel",
        name = addon.name,
        displayName = addon.title,
        author = addon.author,
        version = addon.version,
        registerForRefresh = true,
        registerForDefaults = true
    }

    LibAddonMenu2:RegisterAddonPanel(addon.name .. "Options", panelData)
end

local function SetupSettingsOptions()
    local optionsTable = {
        {
            type = "dropdown",
            name = GetString(BANKEE_SI_SETTINGS_DIRECTION_TITLE),
            tooltip = GetString(BANKEE_SI_SETTINGS_DIRECTION_TOOLTIP),
            choices = {
                GetString(BANKEE_SI_STACK_TO_BANK),
                GetString(BANKEE_SI_STACK_TO_BACKPACK)
            },
            choicesValues = { addon.StackDirection.TO_BANK, addon.StackDirection.TO_BACKPACK },
            getFunc = function() return addon.settings.direction end,
            setFunc = function(var) addon.settings.direction = var end,
        },
        {
            type = "checkbox",
            name = GetString(BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TITLE),
            tooltip = GetString(BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TOOLTIP),
            getFunc = function() return addon.settings.printToChat end,
            setFunc = function(var) addon.settings.printToChat = var end
        },
        {
            type = "submenu",
            name = GetString(BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES),
            controls = {
                {
                    type = "description",
                    text = GetString(BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES_DESCRIPTION)
                },
                {
                    type = "custom",
                    createFunc = addon.classes.CategorySelectionSettings.BuildCategorySelectionOptions,
                    maxHeight = 2000
                }
            }
        }
    }

    LibAddonMenu2:RegisterOptionControls(addon.name .. "Options", optionsTable)
end

function addon.classes.Settings.Initialize()
    SetupSavedVars()
    SetupSettingsPanel()
    SetupSettingsOptions()
end