Bankee = Bankee or {}

Bankee.StackDirection = {
    TO_BANK = 1,
    TO_BACKPACK = 2
}

Bankee.Categories = {
    BLACKSMITHING = 1,
    CLOTHIER = 2,
    WOODWORKING = 3,
    JEWELRY = 4,
    ALCHEMY = 5,
    ENCHANTING = 6,
    PROVISIONING = 7,
    STYLEITEMS = 8,
    TRAITITEMS = 9
}

Bankee.SubCategories = {
    ALL = 1,
    RAW_MATERIAL = 2,
    MATERIAL = 3,
    BOOSTER = 4
}

Bankee.StackOptions = {
    ALL_ITEMS = 1,
    FULL_STACK = 2
}