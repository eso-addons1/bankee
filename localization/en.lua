-- English localization.
-- This is the default and used as a fallback if a string for another language
-- is not defined.

Bankee_Localization = Bankee_Localization or {}

Bankee_Localization["en"] = {
    BANKEE_SI_MOVED_ITEM_MESSAGE = "Moved <<t:1>>: <<2>>/<<3>>",
    BANKEE_SI_STACK_TO_BANK = "Stack to Bank",
    BANKEE_SI_STACK_TO_BACKPACK = "Stack to Backpack",
    BANKEE_SI_SETTINGS_DIRECTION_TITLE = "Stacking Direction",
    BANKEE_SI_SETTINGS_DIRECTION_TOOLTIP = "Determines the bag that items are stacked to.",
    BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TITLE = "Print moved items to chat",
    BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TOOLTIP = "If active, each items moved to the target bag is printed to the chat window",
    BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES = "Additional Categories",
    BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES_DESCRIPTION = "In addition to filling existing stacks, items of the categories configured below will always be moved to the target container. This may even move full stacks.",
    BANKEE_SI_SETTINGS_SUBCATEGORY_ALL = "All",
    BANKEE_SI_SETTINGS_SUBCATEGORY_BOOSTERS = "Boosters"
}
