-- German localization.

Bankee_Localization = Bankee_Localization or {}

Bankee_Localization["de"] = {
    BANKEE_SI_MOVED_ITEM_MESSAGE = "<<t:1>> verschoben: <<2>>/<<3>>",
    BANKEE_SI_STACK_TO_BANK = "In die Bank stapeln",
    BANKEE_SI_STACK_TO_BACKPACK = "In den Rucksack stapeln",
    BANKEE_SI_SETTINGS_DIRECTION_TITLE = "Stapelrichtung",
    BANKEE_SI_SETTINGS_DIRECTION_TOOLTIP = "Bestimmt, wohin die Gegenstände gestapelt werden.",
    BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TITLE = "Chateintrag beim Stapeln",
    BANKEE_SI_SETTINGS_PRINT_TO_CHAT_TOOLTIP = "Wenn diese Option aktiviert ist, wird für jeden verschobenen Gegenstand eine Nachricht in das Chatfenster geschrieben.",
    BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES = "Zusätzliche Kategorien",
    BANKEE_SI_SETTINGS_ADDITIONAL_CATEGORIES_DESCRIPTION = "Zusätzlich zum Füllen von vorhandenen Stapeln werden Gegenstände der folgenden Kategorien immer verschoben. Das betrifft auch volle Stapel.",
    BANKEE_SI_SETTINGS_SUBCATEGORY_ALL = "Alle",
    BANKEE_SI_SETTINGS_SUBCATEGORY_BOOSTERS = "Verstärker"
}
