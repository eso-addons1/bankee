## APIVersion: 100033
## Title: Bankee
## Version: 1.00
## AddOnVersion: 1
## Author: Thenedus
## Description: Stacks items to bank or backpack.
## SavedVariables: Bankee_Data
## DependsOn: LibBag LibAddonMenu-2.0 LibLanguage

localization/en.lua
localization/$(language).lua

BankeeEnums.lua
Bankee.lua

classes/CategorySelectionSettings.lua
classes/Settings.lua
classes/KeyStripButton.lua
classes/ItemUtils.lua
classes/BankStacker.lua
classes/BankMover.lua
